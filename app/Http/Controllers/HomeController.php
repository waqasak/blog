<?php
/**
 * Created by PhpStorm.
 * User: W8
 * Date: 23/05/2016
 * Time: 10:01 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Computer;

class HomeController extends Controller
{

    public function __construct()
    {
    }

    public function storeIt(Request $request)
    {
        // Improved the code, thanks to @boynet for the idea!
        $computer = new Computer;
        $computer->name = $request->get("name");

        $computer->save();

        return redirect()->route('success');
    }

    public function success()
    {
//        return view('private-pages.companyArrivalList',compact('arrival', 'shoe'));
        return view('success');
    }
    public function showComputers()
    {

        $computer = Computer::all();

        return view('list')->with('computer', $computer);

    }
    public function deleteBrand($id)
    {

        $brand = Computer::find($id);

        $brand->delete();
//        return redirect()-> route('companyRegisterBrands');
        return back();
    }
}