<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('child', function () {
    return view('child', ['name' => 'haji']);
});
Route::get('user/profile', [
    'as' => 'profile', 'uses' => 'UserController@showProfile'
]);
Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});

Route::get('deleteBrand/{id}', ['as' => 'deleteBrand', 'uses' => 'CompanyController@deleteBrand']);

Route::post('storeIt',['as' => 'storeIt', 'uses' => 'HomeController@storeIt']);
Route::any('success', ['as' => 'success', 'uses' => 'HomeController@success']);
Route::get('list', ['as' => 'list', 'uses' => 'HomeController@showComputers']);
Route::get('deleteBrand/{id}', ['as' => 'deleteBrand', 'uses' => 'HomeController@deleteBrand']);